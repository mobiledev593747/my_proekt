package com.example.redalert;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.redalert.addressees.doma;
import com.example.redalert.valutess.valutes;


public class MainActivity extends AppCompatActivity {
    LinearLayout L_banks;
    LinearLayout L_money;
    private void initData_banks(){
        L_banks = findViewById(R.id.banks);
    }
    private void initData_money(){
        L_money = findViewById(R.id.lay_money);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData_banks();
        L_banks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent act1 = new Intent(MainActivity.this, doma.class);
                startActivity(act1);
            }
        });
        initData_money();
        L_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent act = new Intent(MainActivity.this, valutes.class);
                startActivity(act);
            }
        });
    }
    public void onClickSignIN(View view){
        FragmentManager manager = getSupportFragmentManager();
        DialogFragment nF = new MyDialogF();
        nF.show(manager,"lol");
    }

}
