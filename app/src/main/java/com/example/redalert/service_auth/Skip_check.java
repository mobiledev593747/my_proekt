package com.example.redalert.service_auth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Skip_check {
    @POST("api/skip")
    //Аннотация @ Body к параметру метода говорит Retrofit
        // использовать объект в качестве тела запроса для вызова.
    Call<skip> getSkip(@Body skip skipi);
}
