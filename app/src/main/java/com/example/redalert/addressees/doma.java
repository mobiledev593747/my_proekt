package com.example.redalert.addressees;


import android.app.ListActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;


import com.example.redalert.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class doma extends ListActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    // нужные переменные

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doma_design);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (mapFragment == null) {
            Toast toast = Toast.makeText(getBaseContext(),"Карта не найдена",Toast.LENGTH_SHORT);
            toast.show();
            return;
        }//проверка наличия карты

        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl("http://intelligent-system.online/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        addressS api = retrofit.create(addressS.class);
        api.getData().enqueue(new Callback<Dom_alot>() {
            @Override
            public void onResponse(Call<Dom_alot> call, Response<Dom_alot> response) {
                if (response.isSuccessful()) {
                    //setDataToList(response.body().getList());
                    setListAdapter(new MyClassAdapter_ad(doma.this,response.body().getList()));
                } else {
                    TextView tvv = findViewById(R.id.address_title);
                    tvv.setText(response.message());
                }
            }
            @Override
            public void onFailure(Call<Dom_alot> call, Throwable t) {
                TextView tv = findViewById(R.id.address_title);
                tv.setText("Fail");
            }
        });

    }

    void setDataToList(List<Dom> sss){
        setListAdapter(new MyClassAdapter_ad(this,sss));
    }

    public void onMapReady(GoogleMap map) {
        this.mMap = map;
        this.mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.addMarker(new MarkerOptions().position(latLng));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
                mMap.animateCamera(cameraUpdate);
            }
        });
    }
}
