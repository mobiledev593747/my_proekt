package com.example.redalert.addressees;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.redalert.R;

import java.util.List;

class MyClassAdapter_ad extends ArrayAdapter<Dom> {
    private final Activity context;
    private final List<Dom> l_d;
    public MyClassAdapter_ad(Activity context, List<Dom> domes) {
        super(context, R.layout.doma_item,domes);
        this.context=context;
        this.l_d=domes;
    }
    static class ViewHolder {
        public TextView ad;
        public TextView te;
        public TextView ts;
        public TextView tup;
        ViewHolder(View view){
            this.ad = view.findViewById(R.id.ad_d);
            this.te = view.findViewById(R.id.t_e_d);
            this.ts = view.findViewById(R.id.t_s_d);
            this.tup = view.findViewById(R.id.t_tip);
        }
        void setData(Dom dom){
            this.ad.setText(dom.getAd());
            this.te.setText(dom.getT_E());
            this.ts.setText(dom.getT_S());
            this.tup.setText(dom.getTip());
        }
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyClassAdapter_ad.ViewHolder holder;
        View rowView;
        LayoutInflater inflater = context.getLayoutInflater();
        rowView = inflater.inflate(R.layout.doma_item, null, true);
        holder = new MyClassAdapter_ad.ViewHolder(rowView);
        holder.setData(l_d.get(position));
        return rowView;
    }
}
