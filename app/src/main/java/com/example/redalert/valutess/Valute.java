package com.example.redalert.valutess;



import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root (name = "Valute")
public class Valute {
    @Element(name = "Value")
    private String value;
    @Element(name = "Name")
    private String name;
    @Element(name = "CharCode")
    private String charcode;
    @Element(name = "Nominal")
    public String nominal;
    @Element(name = "NumCode")
    public String numcode;

    @Attribute(name = "ID")
    public String id;

    public String getValue(){
        return value;
    }
    public String getName(){
        return name;
    }
    public String getCharCode() {
        return charcode;
    }
    public String getNumcode(){return numcode;}
}
