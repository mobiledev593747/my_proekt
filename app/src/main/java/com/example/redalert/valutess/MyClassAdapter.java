package com.example.redalert.valutess;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.redalert.R;
import com.example.redalert.valutess.Valute;

import java.util.List;


public class MyClassAdapter extends ArrayAdapter<Valute> {
    private final Activity context;
    private final List<Valute> listValute;

    public MyClassAdapter(Activity context, List<Valute> valute) {
        super(context, R.layout.valute_item, valute);
        this.context = context;
        this.listValute = valute;
    }

    static class ViewHolder {
        public TextView name;
        public TextView code;
        public TextView value;
        public TextView numcode;
        ViewHolder(View view){
            this.name = view.findViewById(R.id.name_val);
            this.code = view.findViewById(R.id.code_val);
            this.value = view.findViewById(R.id.value_val);
            this.numcode = view.findViewById(R.id.val_nc);
        }
        void setData(Valute v){
            this.name.setText(v.getName());
            this.code.setText(v.getCharCode());
            this.value.setText(v.getValue());
            this.numcode.setText(v.getNumcode());
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView;
        LayoutInflater inflater = context.getLayoutInflater();
        rowView = inflater.inflate(R.layout.valute_item, null, true);
        holder = new ViewHolder(rowView);
        holder.setData(listValute.get(position));
        return rowView;
    }
}
