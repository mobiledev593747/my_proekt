package com.example.redalert.valutess;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface server11 {
    @GET("scripts/XML_daily.asp")
    Call<ValCurs> getData(@Query("data_req") String data_req);
}
